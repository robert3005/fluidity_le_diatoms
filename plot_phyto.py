#!/usr/bin/env python

import sys
import shutil
from numpy import loadtxt
from matplotlib.dates import date2num, num2date
from datetime import datetime, timedelta
from bioplotter import BiologyPlotter1D, find_peak, sample_by_day, sample_date_range
from fluidity_tools import stat_parser
from matplotlib import rcParams
import numpy as np
import matplotlib as mpl

# times and dates
sim_start = "2005-01-01"
sim_finish = "2025-12-31"

plot_start = "2005-01-01"
plot_finish = "2025-01-01"

### Phyto plots
def add_phyto_data(dir, pl_plot, style, label, vtu_min=0, vtu_max=-1, vtu_spacing=1, dim=1):
    pl_data = BiologyPlotter1D(dim, dir)
    pl_data.set_times( sim_start, plot_start, plot_finish, interval=1 )
    if pl_plot.figure_ids.has_key('Phyto'):
        pl_data.add_scalar_field('DiatomEnsembleSizeLiving', col_int=True, isP0=True)

    pl_data.get_mld_data(mld_field='WBDensity', vtu_offset=vtu_min, vtu_max=vtu_max, vtu_spacing=vtu_spacing)
    column = pl_data.col_integrals    
    mldata = pl_data.mld_integrals
    mld = pl_data.mld_data

    if pl_plot.figure_ids.has_key('Phyto'):
        pl_plot.add_summary_data('Phyto', column['DiatomEnsembleSizeLiving'], pl_data.mld_time, style=style, label=label)


def plot_phyto_baseline():
    print "Plotting Phytoplankton baseline"
    global sim_start
    global plot_start
    global plot_finish
    sim_start = "0001-01-01"
    plot_start = "0001-01-01"
    plot_finish = "0003-01-01"

    plphyto = BiologyPlotter1D(1, './')
    plphyto.set_figure_format( figsize=(22,6), axis_dim=[.04,.1,.94,.86], nlevels=64, dpi=180 )
    plphyto.set_times( sim_start, plot_start, plot_finish, interval=1 )
    plphyto.create_summary('Phyto', dpi=180)

    add_phyto_data('./', plphyto, '-k', 'Fluidity-ICOM', vtu_max=2*1460, vtu_spacing=4*5)

    plphyto.get_vtus(dir='./')
    plphyto.print_summary('Phyto', xlbl='Time (Years)', ylbl='Phytoplankton diatom $m^{-2}$')


#################
plot_phyto_baseline()
