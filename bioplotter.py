#!/usr/bin/env python
import matplotlib as mpl
mpl.use("Agg")
# Above is so we don't need an X-screen to save figures
import string
import re
import os
import gc
import vtktools
import numpy as np
from numpy import argsort, size
from pylab import figure, colorbar, arange, date2num, mpl, xlabel, ylabel, savefig, close, legend, gca, clabel
from datetime import datetime, timedelta
from matplotlib.colors import LogNorm
from matplotlib import rcParams
from matplotlib import ticker
from matplotlib import rc

#### taken from http://www.codinghorror.com/blog/archives/001018.html    #######
def sort_nicely( l ):
    """ Sort the given list in the way that humans expect.
    """
    convert = lambda text: int(text) if text.isdigit() else text
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ]
    l.sort( key=alphanum_key )

##############################################################################

def convert_unstructured_2d_array(array):
    """ Fill the individual arrays (in case we have adaptivity)
        to the size of the largest one and convert to numpy array
    """
    max_len = 0
    for i in range(len(array)):
        max_len = max(max_len, len(array[i]))

    new_array = np.zeros( (len(array),max_len) )
    for i in range(len(array)):
        new_array[i, 0:len(array[i])] = array[i][:]
        new_array[i, len(array[i]):-1] = array[i][-1]
    return new_array

def get_1d_indices(pos, x0=0, y0=0, iz=2, tolerance=1.0e-5):
    """ Return the field indices corresponding to the ordered depth values at position (x0, y0)
    """
    ind = argsort(-pos[:,iz])
    indices = []
    for i in ind:
        if (x0-tolerance < pos[i][0] < x0+tolerance and y0-tolerance < pos[i][1] < y0+tolerance):
            indices.append(i)
    return indices

#### Mixed layer depth

def calc_mld_threshold(data, depth, threshold=1.0e-5):
    """ Calculate Mixed Layer depth based on a threshold value
    """
    zd = zip(depth, data)
    inside = lambda (z,d): d >= threshold
    ml_zd = filter(inside, zd)
    ml_depth, ml_data = zip(*ml_zd)
    return np.array(map(abs,ml_depth)).max()

def calc_mld_relative(data, depth, rtol=0.000001, lesser=True):
    """ Calculate Mixed Layer depth based on a relative threshold
    """
    if lesser:
        threshold = data[0] - (rtol * data[0])
        zd = zip(depth, data)
        inside = lambda (z,d): d >= threshold
    else:
        threshold = data[0] + (rtol * data[0])
        zd = zip(depth, data)
        inside = lambda (z,d): d <= threshold
    ml_zd = filter(inside, zd)
    ml_depth, ml_data = zip(*ml_zd)
    return np.array(map(abs,ml_depth)).max()

### Day and peak sampling
def sample_by_day(data, dates, day, month):
    out_data = []
    out_dates = []
    for i in range(len(dates)):
        if dates[i].month == month and dates[i].day == day:
            out_data.append( data[i] )
            out_dates.append( dates[i] )
    return out_data, out_dates

def sample_date_range(data, dates, start, end):
    start_yday = datetime.strptime(start, "%Y-%m-%d").timetuple().tm_yday
    end_yday = datetime.strptime(end, "%Y-%m-%d").timetuple().tm_yday
    out_data = []
    out_dates = []
    for i in range(len(dates)):
        yday = dates[i].timetuple().tm_yday
        if yday >= start_yday and yday <= end_yday:
            out_data.append( data[i] )
            out_dates.append( dates[i] )
    return out_data, out_dates

def find_peak(data, dates, finish_date=None):
    # First separate years
    ydates = {}
    ydata = {}
    for i in range(len(dates)):
        year = dates[i].year

        if finish_date and dates[i] > finish_date:
            continue

        if not ydates.has_key( year ):
            ydates[year] = []
            ydata[year] = []

        ydates[year].append( dates[i] )
        ydata[year].append( data[i] )

    out_data = []
    out_dates = []
    for year in ydates.keys():
        peak = np.argmax( np.array( ydata[year] ) )
        #print "Peak is: " + str(ydata[year][peak]) + " at " + str(ydates[year][peak])
        out_data.append( ydata[year][peak] )
        out_dates.append( ydates[year][peak] )
#    out = zip(out_dates, out_data)
#    out.sort()
#    out_dates, out_data = zip(*out)
    return np.array(out_data), out_dates


##############################################################################
#### Plotter class
##############################################################################
# Global ID counter to identify figures
figure_id_count = 0
class BiologyPlotter1D:
    """ Utility class to plot biology simulation data """

    def __init__(self, dim, folder, adaptive=False):
        global figure_id_count
        self.dim = dim
        self.folder = folder
        self.adaptive = adaptive

        self.set_figure_format()

        self.scalar_fields = []
        self.vtus = None
        self.stat_file = None
        self.stat_data = None

        self.times = []
        self.depths = []
        self.bin_times = []
        self.bin_depths = []
        self.data = {}
        self.isP0 = {}
        self.get_mld = {}
        self.get_col = {}
        self.mld_time = []
        self.mld_data = []
        self.mld_integrals = {}
        self.col_integrals = {}

        self.sim_start = None
        self.sim_start_date = None
        self.plot_start = None
        self.plot_start_date = None
        self.plot_finish = None
        self.plot_finish_date = None

        self.filename = None
        self.figures = {}
        self.figure_ids = {}
        self.max_value = {}
        self.min_value = {}

        params = {
                  'legend.fontsize': 14,
                  'xtick.labelsize': 14,
                  'ytick.labelsize': 14,
                  'font.size' : 18,
                  'axes.labelsize' : 18,
                  'font.size' : 18,
                  'text.fontsize' : 18,
                  'figure.subplot.top' : 0.96,
                  'figure.subplot.left' : 0.08,
                  'figure.subplot.hspace' : 0.4,
                  'text.usetex' : True,
                  'font.family' : 'sans-serif',
                  'font.sans-serif' : ['Helvetica'],
                  'mathtext.rm' : 'sans',
                  'mathtext.fontset' : 'custom',
                  'mathtext.default' : 'sf' #'regular',
        }
        rcParams.update(params)
        #rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
        #rc('mathtext',default='regular')
        #rc('text',usetex=True)

    def set_times(self, sim_start, plot_start, plot_finish, format="%Y-%m-%d", interval=1):
        self.sim_start = sim_start
        self.sim_start_date = datetime.strptime(sim_start, format)
        self.plot_start = plot_start
        self.plot_start_date = datetime.strptime(plot_start, format)
        self.plot_finish = plot_finish
        self.plot_finish_date = datetime.strptime(plot_finish, format)
        self.interval = interval

    def set_figure_format(self,figsize=(15,8), dpi=60, axis_dim=[.05,.12,.95,.85], nlevels=32):
        self.figsize = figsize
        self.dpi = dpi
        self.axis_dim = axis_dim
        self.nlevels = nlevels

    def add_scalar_field(self, sfield, isP0=False, mld_int=False, col_int=False):
        if not sfield in self.scalar_fields:
            self.scalar_fields.append(sfield)
            self.data[sfield] = []
            self.isP0[sfield] = isP0

        if mld_int:
            self.get_mld[sfield] = mld_int
            self.mld_integrals[sfield] = []

        if col_int:
            self.get_col[sfield] = col_int
            self.col_integrals[sfield] = []

    def get_vtus(self, dir=None):
        """ For a given directory, list all VTU files, except checkpoint & _0
        """
        if not dir:
            dir = self.folder
            self.depths = []
        self.folder = dir
        print "Locating vtu files in: " + str(self.folder)
        self.vtus = []
        for file in os.listdir(self.folder):
            if (string.find(file, 'checkpoint') != -1):
                continue
            elif (string.find(file, '.vtu') != -1):
                self.vtus.append(self.folder + "/" + file)
            elif (string.find(file, '.pvtu') != -1):
                self.vtus.append(self.folder + "/" + file)
            else:
                continue
        sort_nicely(self.vtus)

    def get_stat(self, dir=None):
        if not dir:
            dir = self.folder
        self.folder = dir
        for file in os.listdir(self.folder):
            if (string.find(file, '.stat') != -1):
                self.stat_file = self.folder + "/" + file
                break
        return self.stat_file

    def read_depths(self, u=None):
        if not u:
            u = vtktools.vtu( self.vtus[0] )
        pos = u.GetLocations()
        return np.array( sorted(list(set(-pos[:,self.dim-1]))) )

    def get_coordinates(self, depths=None, x=0.0, y=0.0):
        if depths==None:
            if not self.vtus:
                self.get_vtus()
            depths = self.read_depths()
            #self.depths.append( depths )

        coords = None
        if self.dim==1:
            coords = np.array( [ (-z, 0., 0.) for z in depths] )
        elif self.dim==3:
            coords = np.array( [ (x, y, -z) for z in depths] )
        else:
            print "Unknown dimension: " + str(self.dim)
            sys.exit(1)
        return coords

    def bin_p0_data(self, u, field, depths=None, tolerance=0.000001):
        """ 3-dimensional P0-fields are a bit special... """
        if depths==None:
            depths = self.read_depths()

        cells = u.GetScalarField(field)
        bins = np.zeros( len(depths) )
        for i in range(len(cells)):
            bounds = u.GetCellBounds(i)
            bin_depth = min( abs(bounds[-2]), abs(bounds[-1]) )
            vmin = abs(bounds[-1])
            vmax = abs(bounds[-2])
            vdist = vmax - vmin
            value = cells[i] * u.GetCellVolume(i) / vdist

            # Alternative that honours tolerance
            bin_big = np.nonzero( depths > vmin - tolerance )[0]
            ### Use "- tolerance" in the next line to avoid adding
            ### contributions from two cells that shared the depth vertex
            bin_small = np.nonzero( depths < vmax - tolerance )[0]
            bin_indices = np.intersect1d(bin_big, bin_small)
            #bin_index = np.intersect1d(bin_big, bin_small).min()

            for i in bin_indices:
                bins[i] = bins[i] + value
            #bin_index = np.where( depths == bin_depth )[0].min()
            #bins[bin_index] = bins[bin_index] + value

        bins[-1] = bins[-2]
        return bins

    def get_data(self, vtu_spacing=1, vtu_max=-1, vtu_offset=0, vres=0.5):
        if not self.vtus:
            self.get_vtus()

        self.bin_depths = np.arange(0., 300., vres)

        print "Reading .vtu data..."
        for file in self.vtus[vtu_offset:vtu_max:vtu_spacing]:
            try:
                os.stat(file)
            except:
                print "No such file: %s" % file
                sys.exit(1)

            u = vtktools.vtu(file)
            time = self.sim_start_date + timedelta(seconds=u.GetScalarField('Time')[0])
            self.depths.append( self.read_depths(u) )
            self.times.append( np.array([ date2num(time) for i in range( len(self.depths[-1]) ) ]) )
            self.bin_times.append( time )

            for field in self.scalar_fields:
                if self.isP0[field] and self.dim==3:
                    field_data = self.bin_p0_data(u, field, depths=self.bin_depths)
                    #print "ml805 field_data", field_data
                    self.data[field].append( field_data )
                else:
                    coordinates = self.get_coordinates( self.depths[-1] )
                    field_data = u.ProbeData(coordinates, field)[:,0]
                    self.data[field].append( field_data )

        # Fill arrays (in case we have adaptivity)
        for field in self.scalar_fields:
            if self.isP0[field] and self.dim==3:
                self.data[field] = np.array( self.data[field] ).T
                self.bin_times = np.array( self.bin_times )
            else:
                self.data[field] = convert_unstructured_2d_array( self.data[field] )
                self.times = convert_unstructured_2d_array( self.times )
                self.depths = convert_unstructured_2d_array( self.depths )

    def get_mld_data(self, mld_field='ScalarDiffusivity', vtu_spacing=1, vtu_max=-1, vtu_offset=0):
        if not self.vtus:
            self.get_vtus()

        print "Reading .vtu data for ML integration..."
        for file in self.vtus[vtu_offset:vtu_max:vtu_spacing]:
            try:
                os.stat(file)
            except:
                print "No such file: %s" % file
                sys.exit(1)

            u = vtktools.vtu(file)
            self.mld_time.append( self.sim_start_date + timedelta(seconds=u.GetScalarField('Time')[0]) )
            coordinates = self.get_coordinates()
            ml_data = u.ProbeData(coordinates, mld_field)[:,0]
            #mld = calc_mld_threshold(ml_data, coordinates[:,self.dim-1])
            mld = calc_mld_relative(ml_data, coordinates[:,self.dim-1], lesser=False)
            self.mld_data.append ( mld )

            for field in self.scalar_fields:
                if self.isP0[field]:
                    u.CellDataToPointData()

                # Column integral from vtu
                if self.get_col.has_key(field):
                    integral = u.GetFieldIntegral(field)
                    self.col_integrals[field].append( integral )


            for field in self.scalar_fields:
                if self.get_mld.has_key(field):
                    # Crop the field
                    if self.dim == 1:
                        u.Crop(-abs(mld), 0., 0., 0., 0., 0.)
                    else:
                        bbox = u.ugrid.GetBounds()
                        u.Crop( bbox[0], bbox[1], bbox[2], bbox[3], -abs(mld), bbox[5] )
                        #raise Exception('DimException', 'MLD integral not  implemented for dim=3')

                    integral = u.GetFieldIntegral(field)
                    self.mld_integrals[field].append( integral )

        # Numpy-ise
        for field in self.scalar_fields:
            if self.mld_integrals.has_key(field):
                self.mld_integrals[field] = np.array( self.mld_integrals[field] )
            if self.col_integrals.has_key(field):
                self.col_integrals[field] = np.array( self.col_integrals[field] )


    def plot_scalar_contour(self, field, filename, axis_label, max_depth=150, minimum=None, maximum=None, x_spacing=1, x_offset=0, locator=None, formatter=None, lbl_xaxis='Date (mm/yyyy)', stats=False):
        if self.isP0[field] and self.dim==3:
            data = self.data[field][:,x_offset::x_spacing]
            time = self.bin_times[x_offset::x_spacing]
            depth = self.bin_depths
        else:
            print x_offset
            print x_spacing
            self.times.shape
            data = self.data[field][:,x_offset::x_spacing]
            time = self.times[:,x_offset::x_spacing]
            depth = self.depths[:,x_offset::x_spacing]

        if stats:
            print "Averages of vertical profile stats:"
            print "Mean: " + str(np.average( np.mean(data, axis=1) ))
            print "Std. Dev.: " + str(np.average( np.std(data, axis=1) ))
            print "Variance: " + str(np.average( np.var(data, axis=1) ))

        #print filename + ", shapes:: data:" + str(data.shape) + " depth:" + str(depth.shape) + " time:" + str(time.shape)

        # define min/max and spacing of data (+/- 10%, so we see all of the data)
        if (minimum == None):
            minimum = data.min()
            minimum = minimum - (0.1*minimum) - 1.0e-9
        if (maximum == None):
            maximum = data.max()
            maximum = maximum + (0.1*maximum)
        spacing = (maximum - minimum) / self.nlevels

        self.scalar_contour(data, time, depth, filename, axis_label, max_depth, minimum, maximum, spacing, locator=locator, formatter=formatter, lbl_xaxis=lbl_xaxis)

    def scalar_contour(self, data, time, depth, filename, axis_label, max_depth, minimum, maximum, spacing, lvls=None, locator=None, formatter=None, lbl_xaxis='Date (mm/yyyy)'):
        print filename + ", creating figure..."
        fig = figure(figsize=self.figsize, dpi=self.dpi)
        ax = fig.add_axes(self.axis_dim)
        #lvls = [-1.e-9, 10., 25., 50., 100., 250., 500., 1000., 2000. ]
        if lvls:
            cs = ax.contour(time, depth, data, lvls, colors='k') #arange(minimum,maximum,spacing))
            cs = ax.contourf(time, depth, data, lvls, norm=mpl.colors.LogNorm())
        else:
            cs = ax.contour(time, depth, data, arange(minimum,maximum,spacing))
            cs = ax.contourf(time, depth, data, arange(minimum,maximum,spacing))
        pp=colorbar(cs)

        if not locator:
            locator = mpl.dates.MonthLocator(interval=self.interval)
        if not formatter:
            formatter = mpl.dates.DateFormatter('%m/%Y')
        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter(formatter)
        labels = ax.get_xticklabels()
        for label in labels:
            label.set_rotation(30)
        ax.set_ylim(max_depth, 0)
        ax.set_xlim(self.plot_start_date,self.plot_finish_date)
        pp.set_label(axis_label)
        xlabel(lbl_xaxis)
        ylabel('Depth (m)')

        """
        form = filename.split('.')[-1].strip()
        file_path = self.folder + "/" + filename
        print filename + ", saving figure to: " + file_path
        savefig(file_path, dpi=self.dpi, format=form)
        close(fig)
        gc.collect()
        """
        format = 'pdf' #"png"
        file_path = self.folder + "/" + filename + "." + format
        print filename + ", saving figure to: " + file_path
        savefig(file_path, dpi=self.dpi, format=format)
        close(fig)
        gc.collect()


    def create_summary(self, filename, figsize=None, dpi=None):
        global figure_id_count
        if not figsize:
            figsize = self.figsize
        if not dpi:
            dpi = self.dpi
        figure(figure_id_count, figsize=figsize,dpi=dpi)
        self.figure_ids[filename] = figure_id_count
        figure_id_count = figure_id_count + 1
        self.max_value[filename] = 0.0
        self.min_value[filename] = float('inf')

    def add_summary_data(self, filename, data, time, style='-k', label=None, axis_label=None, logscale=False, subplot=None, locator=None):
        fig = figure( self.figure_ids[filename] )
        if not subplot:
            ax = fig.add_axes(self.axis_dim)
        else:
            ax = fig.add_subplot(subplot)
        ax.plot(time, data, style, label=label)
        if logscale:
            ax.set_yscale('log')

        maxval = np.array(data).max()
        maxval += maxval * 0.1
        self.max_value[filename] = maxval

        minval = np.array(data).min()
        minval -= minval * 0.1
        self.min_value[filename] = minval

        if locator:
            dateFmt = mpl.dates.DateFormatter('%b %Y')
            ax.xaxis.set_major_formatter(dateFmt)
            ax.xaxis.set_major_locator(locator)
        else:
            fig.autofmt_xdate()

        if axis_label:
            ylabel(axis_label)
        labels = ax.get_xticklabels()
        for label in labels:
            label.set_rotation(30)
        #ax.set_ylim(self.min_value[filename], self.max_value[filename])
        ax.set_xlim(self.plot_start_date,self.plot_finish_date)
        #xlabel('Date')

    def add_attractor(self, filename, xdata, ydata, style='-k', label=None, xlog=True, ylog=True, xlbl=None, ylbl=None):
        fig = figure( self.figure_ids[filename] )
        ax = fig.add_axes(self.axis_dim)

        ax.plot(xdata, ydata, style, label=label)

        maxval = np.array(ydata).max()
        maxval += maxval * 0.1
        if self.max_value.has_key(filename):
            self.max_value[filename] = max(maxval, self.max_value[filename])
        else:
            self.max_value[filename] = maxval

        minval = np.array(ydata).min()
        minval -= minval * 0.1
        if self.min_value.has_key(filename):
            self.min_value[filename] = min(minval, self.min_value[filename])
        else:
            self.min_value[filename] = minval
        ax.set_ylim(self.min_value[filename], self.max_value[filename])

        if xlog:
            ax.set_xscale('log')

        if ylog:
            ax.set_yscale('log')

        if xlbl:
            xlabel(xlbl)
        if ylbl:
            ylabel(ylbl)


    def add_time_attractor(self, filename, data, time, style='-k', label=None, axis_label=None):
        fig = figure( self.figure_ids[filename] )
        ax = fig.add_axes(self.axis_dim)

        for i in range(len(time)):
            time[i] = time[i].replace(year=1)
        ax.plot(time, data, style, label=label)

        locator = mpl.dates.DayLocator(interval=self.interval)
        dateFmt = mpl.dates.DateFormatter('%b %d')
        ax.xaxis.set_major_formatter(dateFmt)
        ax.xaxis.set_major_locator(locator)
        ax = fig.get_axes()[0]

        if axis_label:
            ylabel(axis_label)
        labels = ax.get_xticklabels()
        for label in labels:
            label.set_rotation(30)
        #ax.set_ylim(self.min_value[filename], self.max_value[filename])
        ax.set_xlim(self.plot_start_date,self.plot_finish_date)
        #xlabel('Date')

    def print_summary(self, filename, xlbl=None, ylbl=None, minval=None, maxval=None, legend_loc=0):

        fig = figure( self.figure_ids[filename] )

        if xlbl:
            xlabel(xlbl)
        if ylbl:
            ylabel(ylbl)
        legend(loc=legend_loc)

        ax = fig.get_axes()[0]
        labels = ax.get_xticklabels()
        #for label in labels:
        #    label.set_rotation(30)

        if(not maxval):
            maxval = self.max_value[filename]
        if(not minval):
            mminval = self.min_value[filename]
        ax.set_ylim(minval, maxval)

        format = 'pdf' #"png"
        file_path = self.folder + "/" + filename + "." + format
        print filename + ", saving figure to: " + file_path
        savefig(file_path, dpi=self.dpi, format=format)
        close(fig)
        gc.collect()

    def plot_1d_comparison(self,data_arr,time_arr,style_arr,label_arr,filename,axis_label,max_value=None,min_value=0.0,logscale=False):

        fig = figure(figsize=self.figsize,dpi=self.dpi)
        ax = fig.add_axes(self.axis_dim)

        for i in range(len(data_arr)):
            ax.plot(time_arr[i],data_arr[i],style_arr[i], label=label_arr[i])

        if(max_value==None):
            max_value = 0.0
            for i in range(len(data_arr)):
                data = vtktools.arr(data_arr[i])
                if data.max() > max_value:
                    max_value = data.max()
            max_value += max_value * 0.1

        if logscale:
            ax.set_yscale('log')

        locator = mpl.dates.MonthLocator(interval=self.interval)
        dateFmt = mpl.dates.DateFormatter('%m/%Y')
        ax.xaxis.set_major_formatter(dateFmt)
        ax.xaxis.set_major_locator(locator)
        labels = ax.get_xticklabels()
        for label in labels:
            label.set_rotation(30)
        ax.set_ylim(min_value, max_value)
        ax.set_xlim(self.plot_start_date,self.plot_finish_date)
        xlabel('Date (mm/yyyy)')
        ylabel(axis_label)
        legend(loc=0)

        form = filename.split('.')[-1].strip()
        file_path = self.folder + "/" + filename
        print filename + ", saving figure to: " + file_path
        savefig(file_path, dpi=self.dpi, format=form)
        close(fig)
        gc.collect()

    def add_vertical_profile(self, filename, field, axis_label='', vtu_index=0, nbins=2, max_depth=150., isP0=False, invert_x=False, style='-k', label=None, stats=False):
        u = vtktools.vtu( self.vtus[vtu_index] )
        if isP0:# and self.dim==3:
            data = self.bin_p0_data(u, field)
        else:
            coordinates = self.get_coordinates()
            print coordinates
            data = u.ProbeData(coordinates, field)[:,0]

        if stats:
            print "Vertical profile stats:"
            print "Mean: " + str(np.mean(data))
            print "Std. Dev.: " + str(np.std(data))
            print "Variance: " + str(np.var(data))

        if invert_x:
            data = data[::-1]

        fig = figure( self.figure_ids[filename] )
        ax = fig.add_axes(self.axis_dim)

        ax.plot(data, self.read_depths(), style, label=label)
        ax.xaxis.set_major_locator(ticker.MaxNLocator(nbins=nbins))
        ax.set_ylim(max_depth, 0.)
        xlabel(axis_label)
        ylabel('Depth (m)')
        legend(loc=0)
