import math 
import numpy as np
from fluidity.state_types import Transform

def get_total_chem(chemfield, xfield):
  total_chem = 0.0
  for e in range(chemfield.element_count):
    t = Transform(e, xfield)
    total_chem += np.dot( chemfield.ele_val_at_quad(e), t.detwei)
  return total_chem

def ml_average(mld, xfield, chemfield):
  """ Averages all elements within the Mixed Layer """
  chem_total = 0.
  max_depth = 0.
  for n in range(chemfield.node_count):
    if abs(xfield.val[n]) < abs(mld):
        chem_total += chemfield.val[n]
        max_depth = max(max_depth, -xfield.val[n+1])
  
  chem_avg = chem_total / max_depth
  for n in range(chemfield.node_count):
    if abs(xfield.val[n]) < abs(mld):
      chemfield.set(n, chem_avg)

def ml_average_3d(mld, xfield, chemfield):
  """ Note: In pseudo-1D, the first element in xfield 
            is at the bottom of the mesh, so we skip it! """

  chem_total = 0.
  max_depth = 0.
  for e in range(1,xfield.element_count):
    depths = [ -xfield.val[n,-1] for n in xfield.ele_nodes(e) ]
    z_min = math.floor( min(depths) )
    if z_min < abs(mld):
      z_max = math.ceil( max(depths) )
      max_depth = max(max_depth, max(depths) )
      # Average over 6 elements per layer and vertical height
      h = max(depths) - min(depths)
      chem_total += chemfield[e-1]*h / 6.

  chem_avg = chem_total / max_depth
  for e in range(1,xfield.element_count):
    depths = [ abs(xfield.val[n,-1]) for n in xfield.ele_nodes(e) ]
    z_min = math.floor( min(depths) )
    if z_min <= abs(mld):
      z_max = math.ceil( max(depths) )
      chemfield.set(e-1, chem_avg )


def conserve_chemical(max_mld, xfield, chem_field, track_field):
  """ Move released quantities from the bottom of the column to the top:
      Note: This assumes 1D mesh with 1m fixed resolution!!!
       - max_mld: Annual maximum Mixed Layer depth
       - chem_field: Dissolved nutrient chemical field (P1DG)
       - track_field: Total accumulated release (P0)
       - xfield: Coordinate field (P1)
  """
  for n in range(track_field.node_count):
    if abs(xfield.val[n,-1]) > abs(max_mld):
      chem_track_0 = min(chem_field.val[2*n], track_field.val[n])
      chem_field.val[0] += chem_track_0
      chem_field.val[2*n] -= chem_track_0
      track_field.val[n] = 0.
      
      chem_track_1 = min(chem_field.val[2*n+1], track_field.val[n])
      chem_field.val[1] += chem_track_1
      chem_field.val[2*n+1] -= chem_track_1
      track_field.val[n] = 0.

def project_to_1m_line(xfield, chemfield, max_depth=500.):
  """ Assume chemfield is P0 chem totals (not concentration) """
  chem_1d = np.zeros( max_depth )
  """ Note: In pseudo-1D, the first element in xfield 
            is at the bottom of the mesh, so we skip it! """
  for e in range(1,xfield.element_count):
    if chemfield.val[e-1] > 0.:
      depths = [ -xfield.val[n,-1] for n in xfield.ele_nodes(e) ]
      z_min = math.floor( min(depths) )
      z_max = math.ceil( max(depths) )
      h = max(depths) - min(depths)
      if (z_max - z_min) <= 1.:
        chem_1d[z_min] += chemfield.val[e-1]*h / 6.
      else:
        chem_1d[z_min] += chemfield.val[e-1] / 6. * ( 1. - (min(depths)-z_min) )
        chem_1d[z_max-1] += chemfield.val[e-1] / 6. * ( 1. - (z_max - max(depths)) )
        chem_1d[z_min+1:z_max-1] += chemfield.val[e-1] / 6.

  return chem_1d

def cache_ele_volume(xfield):
  """ Returns a list of element volumes """
  ele_vol = np.zeros(xfield.element_count)
  # First element is the bottom one...
  for e in range(1,xfield.element_count):
    ele_vol[e-1] = sum( Transform(e, xfield).detwei )
  ele_vol[-1] = sum( Transform(0, xfield).detwei )
  return ele_vol

def get_top_ele(xfield):
  """ Pick some arbitrary element in the top meter """
  for e in range(xfield.element_count):
    for n in xfield.ele_nodes(e):
      if abs(xfield.val[n,-1]) <= 1.:
        return e


def conserve_chemical_3d(max_mld, xfield, chemfield, track_field):
  """ xfield must be Coordintate field """

  print "ml805 total chem before: " + str(get_total_chem(chemfield, xfield))
  
  top_ele = get_top_ele(xfield)
  t = Transform(top_ele, xfield)
  print "ml805 top_ele integral before: ", np.dot( chemfield.ele_val_at_quad(top_ele), t.detwei)

  for e in range(track_field.element_count):
    depths = [ -xfield.val[n,-1] for n in xfield.ele_nodes(e) ]
    z_max = math.ceil( max(depths) )
    if z_max > abs(max_mld):
      # First extract chemicals from below the annual maximum MLD 
      t = Transform(e, xfield)
      chem_integral = np.dot( chemfield.ele_val_at_quad(e), t.detwei)
      chem_track_integral = np.dot( track_field.ele_val_at_quad(e), t.detwei)
      chem_extract = min( chem_track_integral, chem_integral )

      # Correct partially contained elements
      h = max(depths) - min(depths)
      z_min = math.floor( min(depths) )
      if z_min < abs(max_mld):
        chem_extract *= (abs(max_mld) - z_min) / h

      # Extract from current element
      for n in chemfield.ele_nodes(e):
        chemfield.set(n, chemfield.val[n] * (1. - (chem_extract / chem_integral)) )

      # And now re-insert into one of the top-meter elements
      t = Transform(top_ele, xfield)
      chem_integral = np.dot( chemfield.ele_val_at_quad(top_ele), t.detwei)
      for n in chemfield.ele_nodes(top_ele):
        chemfield.set(n, chemfield.val[n] * (1. + (chem_extract / chem_integral)) )

    # Reset the tracking field
    for n in track_field.ele_nodes(e):
      track_field.set(n, 0.)

  print "ml805 total chem after: " + str(get_total_chem(chemfield, xfield))
  t = Transform(top_ele, xfield)
  print "ml805 top_ele integral after: ", np.dot( chemfield.ele_val_at_quad(top_ele), t.detwei)

def event_n_reset(xfield, amm_field, mld, amm_val=12.):
  # Reset Ammonium concentration in the ML to a fixed value
  for e in range(amm_field.element_count):
    n = xfield.ele_nodes(e)[0]
    if abs(xfield.val[n,-1]) <= abs(mld):
      for n in amm_field.ele_nodes(e):
        amm_field.set(n, amm_val)
      

def event_n_reset_3d(xfield, amm_field, mld, amm_val=12.):
  # Reset Ammonium concentration in the ML to a fixed value
  for e in range(1,xfield.element_count):
    depths = [ -xfield.val[n,-1] for n in xfield.ele_nodes(e) ]
    z_min = math.floor( min(depths) )
    z_max = math.ceil( max(depths) )
    h = max(depths) - min(depths)
    if z_min < abs(mld):
      # Average over 6 elements per layer and vertical height
      amm_field.set(e, amm_val)
      #amm_field.set(e, amm_val * h / 6.)
  

####################
###  K-profiles  ###
####################

def visser_curve(z):
    ## Recreate Fig. 1 in Visser, 1997, by using Fig. 3 in Ross and Sharples, 2004
    z = abs(z)
    if z <= 40.:
        z = 40. - z
        K_z = 0.001 + 0.0136245 * z - 0.00263245 * pow(z,2) + 0.000211875 * pow(z,3) - 0.00000865898 * pow(z,4) + 0.00000017623 * pow(z,5) - 0.00000000140918 * pow(z,6)
    else:
        K_z=1e-6
    return K_z
    
def sloped_mu(z, mld, interfacewidth=20., mu_mld=0.3):
    if z <= mld - interfacewidth:
        mu = mu_mld
    elif z <= mld:
        beta = (z - (layer-interfacewidth) ) / interfacewidth
        mu = (1-beta)*mu_mld
    else:
        mu = 0.
    return mu
  
def quadratic_mu(z, mld):
    z = z / mld
    K_z = z - pow(z,2)
    return K_z
  
def cubic_mu(z, mld):
    z = z / mld
    K_z = z * pow( (1-z), 2 ) 
    return K_z
  
def fourth_mu(z, mld):
    z = z / mld
    K_z = z * pow( (1-z), 3 ) 
    return K_z

def popova_mu(z, mld):
    mu_quad = quadratic_mu(z, mld)
    mu_cube = cubic_mu(z, mld)
    
    a = 0.5*( 1 + math.tanh((mld -100.)/500.) )
    mu = a * mu_cube + (1-a) * mu_quad
    return mu



