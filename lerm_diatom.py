from libc cimport math
from lebiology cimport stage_id, add_agent

DEF A_E           = 0
DEF Alpha_Chl     = 1
DEF C_minS        = 2
DEF C_rep         = 3
DEF C_starve      = 4
DEF C_struct      = 5
DEF Ndis          = 6
DEF P_ref_c       = 7
DEF Q_Nmax        = 8
DEF Q_Nmin        = 9
DEF Q_S_max       = 10
DEF Q_S_min       = 11
DEF Q_remN        = 12
DEF Q_remS        = 13
DEF R_Chl         = 14
DEF R_N           = 15
DEF R_maintenance = 16
DEF S_dis         = 17
DEF S_rep         = 18
DEF T_ref         = 19
DEF T_refN        = 20
DEF T_refS        = 21
DEF Theta_max_N   = 22
DEF V_S_ref       = 23
DEF V_ref_c       = 24
DEF Zeta          = 25
DEF k_AR          = 26
DEF k_S           = 27
DEF z_sink        = 28

# Parameters for FGroup Diatom
# Species: Default_Diatom_Variety
cdef double * species_Default_Diatom_Variety = [
   10000.0,
   7.9e-07,
   1.584e-08,
   1.76e-08,
   8.5e-09,
   8.5e-09,
   0.0042,
   0.14,
   0.17,
   0.034,
   0.15,
   0.04,
   2.95,
   2.27,
   0.002,
   0.002,
   0.002,
   0.00083,
   2.1e-09,
   293.0,
   283.0,
   278.0,
   4.2,
   0.03,
   0.01,
   2.3,
   1.0,
   1.0,
   0.04
]

cdef void update_Living_Diatom(double * param, double * vars, double * env, double dt) nogil:
  """ FGroup:  Diatom
      Stage:   Living
  """
  cdef double dt_in_hours = dt / 3600.0

  # pchange_ratio = []
  # pchange_type = []

  ### Effect of temperature ###
  cdef double T_K = (env['Temperature'] + 273.0)
  cdef double T_function = math.exp((param[A_E] * ((1.0 / T_K) - (1.0 / param[T_ref]))))

  ### Photosynthesis ###
  cdef double Q_N = ((vars['Ammonium'] + vars['AmmoniumIngested'] + vars['Nitrate'] + vars['NitrateIngested']) / vars['Carbon'])
  cdef double Q_s = ((vars['Silicate'] + vars['SilicateIngested']) / vars['Carbon'])
  cdef double P_max_c = (((param[P_ref_c] * T_function)) if (Q_N > param[Q_Nmax]) else (((0.0) if (Q_N < param[Q_Nmin]) else ((param[P_ref_c] * T_function * ((Q_N - param[Q_Nmin]) / (param[Q_Nmax] - param[Q_Nmin])))))))
  cdef double Theta_c = (vars['Chlorophyll'] / vars['Carbon'])
  cdef double E_0 = (4.6 * env['Irradiance'])
  cdef double P_phot_c = ((0.0) if ((P_max_c == 0.0) or (Q_s <= param[Q_S_min])) else ((P_max_c * (1.0 - math.exp(((-3600.0 * param[Alpha_Chl] * Theta_c * E_0) / P_max_c))))))

  ### Chlorophyll Synthesis ###
  cdef double Theta_N = (vars['Chlorophyll'] / (vars['Ammonium'] + vars['AmmoniumIngested'] + vars['Nitrate'] + vars['NitrateIngested']))
  cdef double Rho_Chl = (((param[Theta_max_N] * (P_phot_c / (3600.0 * param[Alpha_Chl] * Theta_c * E_0)))) if ((E_0 > 0.0) and (Theta_c > 0.0)) else (0.0))

  ### Respiration ###
  cdef double R_C_growth = (((vars['AmmoniumIngested'] + vars['NitrateIngested']) * param[Zeta]) / (dt_in_hours * vars['Carbon']))
  cdef double R_C = (param[R_maintenance] + R_C_growth)

  ### Cell Division ###
  cdef double C_d = ((2.0) if (((vars['Carbon'] + (vars['Carbon'] * (P_phot_c - R_C) * dt_in_hours)) >= param[C_rep]) and ((vars['Silicate'] + vars['SilicateIngested']) >= param[S_rep])) else (1.0))
  if (C_d == 2.0):
    vars['Size'] = vars['Size'] * 2.0

  ### Nutrients uptake ###
  cdef double Q_nitrate = ((vars['Nitrate'] + vars['NitrateIngested']) / vars['Carbon'])
  cdef double Q_ammonium = ((vars['Ammonium'] + vars['AmmoniumIngested']) / vars['Carbon'])
  cdef double omega = ((param[k_AR] / (param[k_AR] + env['DissolvedAmmonium'])) * ((param[k_AR] + env['DissolvedNitrate']) / (param[k_AR] + env['DissolvedAmmonium'] + env['DissolvedNitrate'])))
  cdef double V_max_C = (((((param[V_ref_c] * T_function)) if ((Q_ammonium + Q_nitrate) < param[Q_Nmin]) else (((0.0) if ((Q_ammonium + Q_nitrate) > param[Q_Nmax]) else ((param[V_ref_c] * math.pow(((param[Q_Nmax] - (Q_ammonium + Q_nitrate)) / (param[Q_Nmax] - param[Q_Nmin])), 0.05) * T_function)))))) if ((vars['Ammonium'] + vars['Nitrate']) < 1000.0) else (0.0))
  cdef double V_C_ammonium = (V_max_C * (env['DissolvedAmmonium'] / (param[k_AR] + env['DissolvedAmmonium'])))
  cdef double V_C_nitrate = (V_max_C * (env['DissolvedNitrate'] / (param[k_AR] + env['DissolvedNitrate'])) * omega)
  cdef double V_S_max = (((((param[V_S_ref] * T_function)) if (Q_s <= param[Q_S_min]) else (((0.0) if (Q_s >= param[Q_S_max]) else ((param[V_S_ref] * math.pow(((param[Q_S_max] - Q_s) / (param[Q_S_max] - param[Q_S_min])), 0.05) * T_function)))))) if (vars['Carbon'] >= param[C_minS]) else (0.0))
  cdef double V_S_S = (V_S_max * (env['DissolvedSilicate'] / (env['DissolvedSilicate'] + param[k_S])))
  vars['AmmoniumUptake'] = (vars['Carbon'] * V_C_ammonium * dt_in_hours)
  vars['NitrateUptake'] = (vars['Carbon'] * V_C_nitrate * dt_in_hours)
  vars['SilicateUptake'] = (vars['Silicate'] * V_S_S * dt_in_hours)

  ### Update Pools ###
  cdef double Ammonium_new = ((((vars['Ammonium'] + vars['AmmoniumIngested'] + vars['NitrateIngested']) - (vars['Ammonium'] * param[R_N] * dt_in_hours * T_function)) - (((0.0 * (Q_N - param[Q_Nmax]))) if (Q_N > param[Q_Nmax]) else (0.0))) / C_d)
  cdef double Nitrate_new = 0.0
  cdef double Silicate_new = (((vars['Silicate'] + vars['SilicateIngested']) - 0.0) / C_d)
  cdef double C_new = max(0.0, (((vars['Carbon'] * (P_phot_c - (R_C * T_function)) * dt_in_hours) + vars['Carbon']) / C_d))
  cdef double death_flag = ((1.0) if (C_new <= param[C_starve]) else (0.0))
  if (death_flag == 1.0):
    vars['Stage'] = stage_id('Diatom', 'Dead')
  cdef double Carbon_new = C_new
  cdef double Chlorophyll_new = ((max((((((vars['Chlorophyll'] + (Rho_Chl * (vars['AmmoniumIngested'] + vars['NitrateIngested'])))) if (Theta_N <= param[Theta_max_N]) else ((vars['Chlorophyll'] - (vars['Chlorophyll'] - ((vars['Ammonium'] + vars['Nitrate']) * param[Theta_max_N]))))) - ((vars['Chlorophyll'] * param[R_Chl] * dt_in_hours * T_function) + 0.0)) / C_d), 0.0)) if (death_flag == 0.0) else (0.0))
  cdef double Nitrogen_new = (vars['Ammonium'] + vars['Nitrate'])
  cdef double C_fuel_new = (vars['Carbon'] - param[C_struct])

  ### Remineralisation Nitrogen ###
  vars['AmmoniumRelease'] = ((vars['Ammonium'] + vars['Nitrate']) * param[R_N] * dt_in_hours * T_function)

  ### Photoadaptation ###
  cdef double ChltoC_new = (((vars['Chlorophyll'] / vars['Carbon'])) if (vars['Carbon'] > 0.0) else (0.0))
  cdef double NtoC_new = ((((vars['Ammonium'] + vars['Nitrate']) / vars['Carbon'])) if (vars['Carbon'] > 0.0) else (0.0))

  ### Setting pool variables
  vars['Ammonium'] = Ammonium_new
  vars['Nitrate'] = Nitrate_new
  vars['Silicate'] = Silicate_new
  vars['Carbon'] = Carbon_new
  vars['Chlorophyll'] = Chlorophyll_new
  vars['Nitrogen'] = Nitrogen_new
  vars['C_fuel'] = C_fuel_new
  vars['ChltoC'] = ChltoC_new
  vars['NtoC'] = NtoC_new

  ### Dealing with pchanges
  # if len(pchange_ratio) > 0:
  #   pchange_total = sum([ 1. if pc > 0. else 0. for pc in pchange_ratio])
  #   for i in range(len(pchange_ratio)):
  #     if pchange_ratio[i] > 0.:
  #       new_agent = {}
  #       for k,v in vars.iteritems():
  #         new_agent[k] = v
  #       new_agent['Stage'] = pchange_type[i]
  #       new_agent['Size'] = vars['Size'] * (pchange_ratio[i] / pchange_total)
  #       vars['Size'] -= new_agent['Size']
  #       if vars.has_key('x') and vars.has_key('y'):
  #         add_agent('Diatom', new_agent, [vars['x'], vars['y'], -vars['z']])
  #       else:
  #         add_agent('Diatom', new_agent, [-vars['z']])

cdef void update_Dead_Diatom(double * param, double * vars, double * env, double dt) nogil:
  """ FGroup:  Diatom
      Stage:   Dead
  """
  cdef double dt_in_hours = dt / 3600.0

  # pchange_ratio = []
  # pchange_type = []

  ### Remineralisation Dead T ###
  cdef double Si_reminT = (param[S_dis] * math.pow(param[Q_remS], (((env['Temperature'] + 273.0) - param[T_refS]) / 10.0)))
  cdef double N_reminT = (param[Ndis] * math.pow(param[Q_remN], (((env['Temperature'] + 273.0) - param[T_refN]) / 10.0)))
  vars['SilicateRelease'] = (vars['Silicate'] * Si_reminT * dt_in_hours)
  cdef double Silicate_new = max((vars['Silicate'] - (vars['Silicate'] * Si_reminT * dt_in_hours)), 0.0)
  vars['AmmoniumRelease'] = ((vars['Ammonium'] + vars['Nitrate']) * N_reminT * dt_in_hours)
  cdef double Ammonium_new = max((vars['Ammonium'] - (vars['Ammonium'] * N_reminT * dt_in_hours)), 0.0)
  cdef double Nitrate_new = max((vars['Nitrate'] - (vars['Nitrate'] * N_reminT * dt_in_hours)), 0.0)

  ### Setting pool variables
  vars['Silicate'] = Silicate_new
  vars['Ammonium'] = Ammonium_new
  vars['Nitrate'] = Nitrate_new

  ### Dealing with pchanges
  # if len(pchange_ratio) > 0:
  #   pchange_total = sum([ 1. if pc > 0. else 0. for pc in pchange_ratio])
  #   for i in range(len(pchange_ratio)):
  #     if pchange_ratio[i] > 0.:
  #       new_agent = {}
  #       for k,v in vars.iteritems():
  #         new_agent[k] = v
  #       new_agent['Stage'] = pchange_type[i]
  #       new_agent['Size'] = vars['Size'] * (pchange_ratio[i] / pchange_total)
  #       vars['Size'] -= new_agent['Size']
  #       if vars.has_key('x') and vars.has_key('y'):
  #         add_agent('Diatom', new_agent, [vars['x'], vars['y'], -vars['z']])
  #       else:
  #         add_agent('Diatom', new_agent, [-vars['z']])
