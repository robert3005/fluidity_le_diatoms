import math
import gzip
import struct
import numpy as np
from scipy import interpolate

""" Taken from: http://stackoverflow.com/questions/42558/python-and-the-singleton-pattern """
class Singleton:
    """
    A non-thread-safe helper class to ease implementing singletons.
    This should be used as a decorator -- not a metaclass -- to the
    class that should be a singleton.

    The decorated class can define one `__init__` function that
    takes only the `self` argument. Other than that, there are
    no restrictions that apply to the decorated class.

    To get the singleton instance, use the `Instance` method. Trying
    to use `__call__` will result in a `TypeError` being raised.

    Limitations: The decorated class cannot be inherited from.

    """

    def __init__(self, decorated):
        self._decorated = decorated

    def Instance(self):
        """
        Returns the singleton instance. Upon its first call, it creates a
        new instance of the decorated class and calls its `__init__` method.
        On all subsequent calls, the already created instance is returned.

        """
        try:
            return self._instance
        except AttributeError:
            self._instance = self._decorated()
            return self._instance

    def __call__(self):
        raise TypeError('Singletons must be accessed through `Instance()`.')

    def __instancecheck__(self, inst):
        return isinstance(inst, self._decorated)

class PLayer:
    def __init__(self, depth):
        self.Depth = depth
        self.Density = 0.0
        self.Temperature = 0.0
        self.TempAdj = 0.0
        self.Salinity = 0.0
        self.Vis_Irrad = 0.0
        self.Full_Irrad = 0.0
        self.irrad = np.zeros((25,))

    def __str__(self):
        s = "WBPhysics::PLayer::Depth:  " + str(self.Depth)
        s += "\n    ::Temperature:  " + str(self.Temperature)
        s += "\n    ::Salinity:  " + str(self.Salinity)
        s += "\n    ::Denisty:  " + str(self.Density)
        s += "\n    ::Irradiance::"
        s += "\n        ::Visual:  " + str(self.Vis_Irrad)
        s += "\n        ::Full:  " + str(self.Full_Irrad)
        return s

class BLayer:
    def __init__(self, depth, height):
        self.Depth = depth
        self.Height = height
        self.particulate_chl = 0.0  # Should later be set by external model
        self.Ammonium = 0.0
        self.Nitrate = 0.0
        self.Silicate = 0.0

    def __str__(self):
        s = "WBPhysics::BLayer::Depth:  " + str(self.Depth)
        s += "\n              ::Height:  " + str(self.Height)
        return s


class Chlorophyll:
    def __init__(self):
        self.param_chi = [0.0, 0.0, 0.112, 0.109, 0.095, 0.077, 0.061, 0.047, 0.041, 0.035, 0.035, 0.041, 0.045, 0.049, 0.034, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
        self.param_e = [1, 1, 0.677, 0.702, 0.702, 0.703, 0.695, 0.673, 0.65, 0.618, 0.628, 0.65, 0.672, 0.687, 0.62, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]

    def chi(self, freq):
        return self.param_chi[freq]

    def e(self, freq):
        return self.param_e[freq]

@Singleton
class WBPhysics:
    """ Python Wrapper for WB physics model """

    def __init__(self, bcd_filename='lerm_bcds.bin', tolerance=12, interpolate=False, verbose=True):
        self.bcd_file = gzip.open(bcd_filename, 'rb')
        self.init_file = gzip.open('lerm_physics_init.bin', 'rb')
        self.interpolate = interpolate  # Switch off interpolation
        self.verbose = verbose

        self.timestep = 0
        self.timestep_size = 1800.0
        self.time = 0.0  #in sec        
        self.dtol = tolerance  #decimal round-off tolerance for layer depths
        self.chlorophyll = Chlorophyll()
        self.MLDepth = 75.
        self.max_mld = 75.
        self.daily_max_mld = 75.

        self.WSpeed = 0.0
        self.Cloud_Cover = 0.0
        self.Cooling = 0.0
        self.Ekman = 0.0
        self.Z_Height = 0.0
        self.SunLight = 0.0
        self.Longitude = 0.0
        self.Latitude = 0.0

        self.Cp = 3940
        self.C1 = 0.072169
        self.C2 = 0.049762
        self.C3 = 0.80560
        self.C4 = -0.0075911
        self.C5 = -0.0030063
        self.C6 = 0.000035187
        self.C7 = 0.000037297
        self.g = 9.81
        self.Pi = 3.141593
        self.Mix_Eff = 0.5
        self.CP_Eff = 0.1
        self.EF_Depth = 100

        self.Salfa = 0.
        self.ZStar = 0.
        self.Alpha = 0.
        self.MixTemp = 0.
        self.ZStar_M = 0.
        self.BStar = 0.
        self.Penerg = 0.
        self.SIGM = 0.
        self.UStarCube = 0.
        self.oddTimeStep = True

        self.b_layers = []
        self.depths = []
        self.ref_depths = []
        self.layers = {}

        self.interp_irrad = None
        self.interp_temp = None
        self.interp_den = None
        self.interp_sal = None

        self.__init_layers()
        if self.interpolate:
            self.__prepare_interp()

    #####  Public functions for model coupling  #####
    def advance(self, current_time):
        """ Advance to the current simulation time """
        
        while current_time > self.time:
            self.__read_bcd()
            self.time = (self.timestep+1) * self.timestep_size
            if self.verbose:
                print "WBPhysics:: timestep: " + str(self.timestep) + "  time: " + str(self.time)

            # Events: N Reset
            if self.timestep < 35040:
                for layer in self.b_layers:
                    if layer.Depth <= self.MLDepth:
                        layer.Ammonium = 12.

            # Events: Temperature correction
            if self.timestep == 17520 or self.timestep == 35040:
                for i in range(len(self.depths)):
                    layer = self.__get_PLayer( self.depths[i] )
                    if layer.Depth <self.MLDepth:
                        layer.TempAdj = 15.2428 - layer.Temperature

            self.update_physics()
            if self.verbose:
                print "WBPhysics:: t: " + str(self.timestep) + "  MLD: " + str(self.MLDepth)
                print "WBPhysics:: n_player: " + str(len(self.depths))

            self.update_chemistry()

            if self.MLDepth > self.max_mld:
                self.max_mld = self.MLDepth

            if self.MLDepth > self.daily_max_mld:
                self.daily_max_mld = self.MLDepth

            # Reset daily max MLD counter
            # (Note: We start at 6:00)
            if (self.timestep % 48 == 36):
                self.daily_max_mld = self.MLDepth


    def getIrradiance(self, z, time=None):
        """ Returns visual irradiance according to embedded Morel model """
        if time:
            self.advance(time)
        layer = self.__get_PLayer(z)
        if layer:
            return layer.Vis_Irrad
        else:
            if self.interpolate:
                return self.interp_irrad(z)
            else:
                raise Exception("WBPhysics:: Cannot find layer for irradiance. Please use interpolation.")

    def setChlorophyll(self, z, chl):
        """ Set particulate chlorophyll at depth z to influence irradiance distribution """
        self.b_layers[ int(math.floor(z)) ].particulate_chl = chl

    def getTemperature(self, z, time=None):
        """ Returns temperature at depth z"""
        if time:
            self.advance(time)
        layer = self.__get_PLayer(z)
        if layer:
            return layer.Temperature + layer.TempAdj
        else:
            if self.interpolate:
                return self.interp_temp(z)
            else:
                raise Exception("WBPhysics:: Cannot find layer for temperature. Please use interpolation.")

    def getSalinity(self, z, time=None):
        """ Returns salinity at depth z"""
        if time:
            self.advance(time)
        layer = self.__get_PLayer(z)
        if layer:
            return layer.Salinity
        else:
            if self.interpolate:
                return self.interp_sal(z)
            else:
                raise Exception("WBPhysics:: Cannot find layer for salinity. Please use interpolation.")

    def getDensity(self, z, time=None):
        """ Returns density at depth z"""
        if time:
            self.advance(time)
        layer = self.__get_PLayer(z)
        if layer:
            return layer.Density
        else:
            if self.interpolate:
                return self.interp_den(z)
            else:
                raise Exception("WBPhysics:: Cannot find layer for density. Please use interpolation.")

    def getAmmonium(self, z, time=None):
        """ Returns dissolved ammonium at depth z"""
        if time:
            self.advance(time)
        return self.b_layers[ int(math.floor(z)) ].Ammonium

    def setAmmonium(self, z, amm):
        """ Set dissolved ammonium at depth z"""
        self.b_layers[ int(math.floor(z)) ].Ammonium = amm

    def getNitrate(self, z, time=None):
        """ Returns dissolved nitrate at depth z"""
        if time:
            self.advance(time)
        return self.b_layers[ int(math.floor(z)) ].Nitrate

    def setNitrate(self, z, nit):
        """ Set dissolved nitrate at depth z"""
        self.b_layers[ int(math.floor(z)) ].Nitrate = nit

    def getSilicate(self, z, time=None):
        """ Returns dissolved silicate at depth z"""
        if time:
            self.advance(time)
        return self.b_layers[ int(math.floor(z)) ].Silicate

    def setSilicate(self, z, sil):
        """ Set dissolved silicate at depth z"""
        self.b_layers[ int(math.floor(z)) ].Silicate = sil

    #####  Internal functions  #####
    def __init_layers(self):
        nlayers = struct.unpack(">i", self.init_file.read(4))[0]
        depths = []
        for i in range(nlayers):
            z = struct.unpack(">d", self.init_file.read(8))[0]
            layer = PLayer(z)
            layer.Temperature = struct.unpack(">d", self.init_file.read(8))[0]
            layer.Salinity = struct.unpack(">d", self.init_file.read(8))[0]
            layer.Density = struct.unpack(">d", self.init_file.read(8))[0]
            self.layers[ round(z, self.dtol) ] = layer
            depths.append(z)

        # Add layer 500.
        finalLayer = PLayer(layer.Depth + 1.)
        finalLayer.Density = layer.Density
        finalLayer.Temperature = layer.Temperature
        finalLayer.Salinity = layer.Salinity
        depths.append( finalLayer.Depth )
        self.layers[ round(finalLayer.Depth, self.dtol) ] = finalLayer

        self.depths = np.around(np.array(depths), decimals=self.dtol)
        self.ref_depths = np.around(np.array(depths), decimals=self.dtol)

        # Add BLayers (1m res)
        self.b_layers = []
        depths, amm_data, sil_data  = np.loadtxt('LERM_InitialConditions.csv', delimiter=',', skiprows=1, unpack=True, usecols=(0,4,5))
        for i in range(len(depths)):
            blayer = BLayer(depths[i], 1.)
            blayer.Ammonium = amm_data[i]
            blayer.Silicate = sil_data[i]
            self.b_layers.append( blayer )

    def __get_PLayer(self, depth):
        z = round(depth, self.dtol)
        if self.layers.has_key(z):
            return self.layers[z]
        else:
            return None

    def __insert_PLayer(self, layer, ind):
        self.depths = np.insert(self.depths, ind, round(layer.Depth, self.dtol), axis=0)
        self.layers[round(layer.Depth, self.dtol)] = layer

    def __read_bcd(self):
        self.timestep = int( struct.unpack(">d", self.bcd_file.read(8))[0] )
        self.WSpeed = struct.unpack(">d", self.bcd_file.read(8))[0]
        self.Cloud_Cover = struct.unpack(">d", self.bcd_file.read(8))[0]
        self.Cooling = struct.unpack(">d", self.bcd_file.read(8))[0]
        self.Ekman = struct.unpack(">d", self.bcd_file.read(8))[0]
        self.Z_Height = struct.unpack(">d", self.bcd_file.read(8))[0]
        self.SunLight = struct.unpack(">d", self.bcd_file.read(8))[0]
        self.Longitude = struct.unpack(">d", self.bcd_file.read(8))[0]
        self.Latitude = struct.unpack(">d", self.bcd_file.read(8))[0]

    def __prepare_interp(self):
        z = []
        irrad = []
        temp = []
        den = []
        sal = []
        i = 0

        for i in range( len(self.depths) ):
            layer = self.__get_PLayer( self.depths[i] )
            z.append( layer.Depth )
            irrad.append( layer.Vis_Irrad )
            temp.append( layer.Temperature + layer.TempAdj )
            den.append( layer.Density )
            sal.append( layer.Salinity )

        self.interp_irrad = interpolate.interp1d(z, irrad, bounds_error=True)
        self.interp_temp = interpolate.interp1d(z, temp, bounds_error=True)
        self.interp_den = interpolate.interp1d(z, den, bounds_error=True)
        self.interp_sal = interpolate.interp1d(z, sal, bounds_error=True)
        
    def update_chemistry(self):
        """ Chemical averaging in the Mixed Layer """

        # Average ML solution chemicals
        amm_total = 0.
        nit_total = 0.
        sil_total = 0.
        mldLayers = 0
        for layer in self.b_layers:
            if layer.Depth < self.MLDepth:
                amm_total += layer.Ammonium
                nit_total += layer.Nitrate
                sil_total += layer.Silicate
                mldLayers += 1
        
        amm = amm_total / mldLayers
        nit = nit_total / mldLayers
        sil = sil_total / mldLayers
        for layer in self.b_layers:
            if layer.Depth < self.MLDepth:
                layer.Ammonium = amm
                layer.Nitrate = nit
                layer.Silicate = sil

    #####  The actual WB model in all its glory  #####
    def update_physics(self):

        layer0 = self.__get_PLayer(0.0)
        UStar = 0.001651 / (layer0.Density + 1000.)
        UStar = math.pow(UStar, 1.5)
        self.UStarCube = UStar * math.pow(self.WSpeed,3)
        self.SIGM = layer0.Density + 1000.0
        MLS = layer0.Salinity
        self.Alpha = (-(self.C2 + (2.0*self.C4*self.MixTemp) + (self.C5*MLS) + (self.C6*self.MixTemp*self.MixTemp) + (2.0*self.C7*MLS*self.MixTemp)) / self.SIGM)
        self.BStar = ((self.Alpha * self.Cooling * self.timestep_size) / self.Cp);

        self.PB_LFlux()
        self.Convection()
        self.Mixing()

        self.MLDepth = self.ZStar_M

        self.Order()

        # Now after everything is done we prepare interpolators
        if self.interpolate:
            self.__prepare_interp()

    def PB_LFlux(self):
        """ Solar Irradiance and Heat flux terms """
        gg = [2.12,2.3,2.71,3.42,3.97,4.21,3.94,3.78,3.78,3.68,3.63,3.52,3.42,3.31,3.15,2.97,2.41,10.17,10.51,6.73,2.52,4.42,1.68,2.10,1.58]
        hh = [0.06,0.045,0.043,0.033,0.019,0.018,0.036,0.041,0.069,0.091,0.186,0.228,0.228,0.367,0.5,1.24,2.4,2.05,20,60,400,500,700,2000,3000]
        specbd = np.zeros((25,))
        nxrad = 0
        DChange = 0.
        self.oddTimeStep = not self.oddTimeStep

        radi = self.SunLight
        if radi > 0.0:
            current = self.__get_PLayer(0.0)
            current.Full_Irrad = 0.
            current.Vis_Irrad = 0.
            for i in range(25):
                current.irrad[i] = 0.

            radi *= 0.0104134
            v = 1 / (math.cos(math.asin(0.75 * math.sin(self.Z_Height))))
            for i in range(25):
                specbd[i] = radi * gg[i]
                current.Full_Irrad += specbd[i]
                current.irrad[i] += specbd[i]
                if i>=2 and i<15:
                    current.Vis_Irrad += specbd[i]
            
            ODepth = current.Depth
            for i in range(1,len(self.depths)):
                current = self.__get_PLayer( self.depths[i] )
                current.Full_Irrad = 0.
                current.Vis_Irrad = 0.
                for j in range(25):
                    current.irrad[j] = 0.

                if nxrad == 0:
                    dx = (ODepth - current.Depth) * v
                    for j in range(25):
                        arg = dx*hh[j];

                        chl_conc = self.b_layers[ int(math.floor(current.Depth)) ].particulate_chl
                        arg += dx * ((self.chlorophyll.chi(j) * math.pow(chl_conc, self.chlorophyll.e(j))));
                        if arg < -10:
                            specbd[j] = 0.
                        else:
                            specbd[j] *= math.exp(arg)
                        current.Full_Irrad += specbd[j]
                        current.irrad[j] += specbd[j]
                        if j>=2 and j<15:
                            current.Vis_Irrad += specbd[j]

                    if current.Full_Irrad < 0.01:
                        current.Full_Irrad = 0.
                        current.Vis_Irrad = 0.
                        for j in range(25):
                            current.irrad[j] = 0.
                        nxrad = 1
                    ODepth = current.Depth

            previous = self.__get_PLayer( self.depths[-1] )
            for i in reversed( range(len(self.depths)-1) ):
                current = self.__get_PLayer( self.depths[i] )
                T = current.Temperature
                SS = current.Salinity
                Sigma = current.Density + 1000
                self.Alpha = -(self.C2 + (2 * self.C4 * T) + (self.C5 * SS) + (self.C6 * T * T) + (2 * self.C7 * SS * T)) / Sigma
                self.Alpha *= (2.0 * self.timestep_size / self.Cp)
                DeltaD = self.Alpha * (current.Full_Irrad - previous.Full_Irrad) / (previous.Depth - current.Depth)
                DeltaD -= DChange
                current.Density -= DeltaD
                DChange = DeltaD
                previous = current

            if self.oddTimeStep:
                previous = self.__get_PLayer( self.depths[-1] )
                for i in reversed( range(len(self.depths)-1) ):
                    current = self.__get_PLayer( self.depths[i] )
                    if current.Density >= previous.Density:
                        current.Density = previous.Density - 0.0000000005
                    previous = current
            else:
                previous = self.__get_PLayer( self.depths[0] )
                for i in range(1,len(self.depths)):
                    current = self.__get_PLayer( self.depths[i] )
                    if current.Density <= previous.Density:
                        current.Density = previous.Density + 0.0000000005
                    previous = current

        else:
            for i in range(len(self.depths)):
                current = self.__get_PLayer( self.depths[i] )
                current.Full_Irrad = 0.
                current.Vis_Irrad = 0.
                for i in range(25):
                    current.irrad[i] = 0.
            

    def Convection(self):
        current = self.__get_PLayer(0.0)
        S = 0.0
        i = 1
        maxI = 1
        while S < self.BStar:
            previous = current
            current = self.__get_PLayer( self.depths[i] )
            S += 0.5 * (current.Depth + previous.Depth) * (current.Density - previous.Density)
            maxI = i
            i += 1

        DS = S - self.BStar
        DZ = current.Depth - previous.Depth
        DeltaT = current.Density - previous.Density
        C = (2 * DS * DZ) / DeltaT
        self.ZStar = math.sqrt((current.Depth * current.Depth) - C)
        TStar = previous.Density - ((DeltaT / DZ) * (previous.Depth - self.ZStar))

        self.Salfa = 0
        previous = self.__get_PLayer(0.0)
        for i in range(1,maxI):
            current = self.__get_PLayer( self.depths[i] )
            self.Salfa += ((0.5 * (current.Density + previous.Density)) - TStar) * (current.Depth - previous.Depth) * (0.5 * (current.Depth + previous.Depth))
            previous = current
        self.Salfa += (((0.5 * (previous.Density + TStar)) - TStar) * (self.ZStar - previous.Depth) * (0.5 * (self.ZStar + previous.Depth)))
        for i in range(maxI):
            self.__get_PLayer(self.depths[i]).Density = TStar

        current = self.__get_PLayer(maxI)
        if ((current.Depth - self.ZStar) >= 0.0001) or ((self.ZStar - previous.Depth) >= 0.0001):
            if (self.ZStar - previous.Depth) < 0.0001:
                newLayer = PLayer(self.ZStar + 0.00005)
                newLayer.Salinity = previous.Salinity
                newLayer.Full_Irrad = previous.Full_Irrad
                newLayer.Vis_Irrad = previous.Vis_Irrad
            elif (current.Depth - self.ZStar) < 0.0001:
                newLayer = PLayer(self.ZStar - 0.00005)
                newLayer.Salinity = current.Salinity
                newLayer.Full_Irrad = current.Full_Irrad
                newLayer.Vis_Irrad = current.Vis_Irrad
            else:
                newLayer = PLayer(self.ZStar)
                newLayer.Salinity = ((current.Salinity - previous.Salinity) * (self.ZStar - previous.Depth) / (current.Depth - previous.Depth)) + previous.Salinity
                newLayer.Full_Irrad = (previous.Full_Irrad + current.Full_Irrad) / 2
                newLayer.Vis_Irrad = (previous.Vis_Irrad + current.Vis_Irrad) / 2
            newLayer.Density = TStar
            newLayer.TempAdj = previous.TempAdj
            self.ZStar = newLayer.Depth

            self.__insert_PLayer(newLayer, maxI)
        else:
            self.ZStar = previous.Depth

    def Mixing(self):
        current = self.__get_PLayer(0.0)
        DeltaE = Flob = 0.
        Finished = i = j = 0        

        EK = (-self.Salfa * self.CP_Eff);
        EW = self.Mix_Eff * self.UStarCube * (current.Density + 1000) * self.timestep_size / self.g;
        #TKE = EW + EK;

        while current.Depth != self.ZStar:
            i += 1
            current = self.__get_PLayer( self.depths[i] )

        while DeltaE <= self.Penerg and Finished == 0:
            current = self.__get_PLayer( self.depths[i] )
            next = self.__get_PLayer( self.depths[i+1] )
            TMix = next.Density
            AFlob = Flob
            Flob += (0.5 * (next.Depth + current.Depth) * (TMix - current.Density))
            currentJ = next
            Flun = 0
            j = i + 1;
            previous = currentJ  #/* Needed for initialisation */

            maxJ = len(self.depths)-1
            while Flun < Flob and j < maxJ:
                previous = currentJ
                j += 1
                currentJ = self.__get_PLayer( self.depths[j] )
                Flun += (currentJ.Density + previous.Density - (2.0 * TMix)) * 0.5 * (currentJ.Depth - previous.Depth)

            if Flun < Flob:
                #raise Exception("WBPhysics: ERROR - Flun<Flob loop has tried to access a layer below bottom of column.")
                if self.verbose:
                    print "WBPhysics: ERROR - Flun<Flob loop has tried to access a layer below bottom of column."

            m = j
            Tj = currentJ.Density
            TjMin = previous.Density
            Zj = currentJ.Depth
            ZjMin = previous.Depth
            Diff = Flun-Flob
            DZ = Zj - ZjMin
            DeltaT = Tj - TjMin
            C = (-2 * Diff * DeltaT) / DZ
            TStar_M = math.sqrt(C + ((Tj - TMix) * (Tj - TMix))) + TMix

            if DeltaT < 0.000001:
                self.ZStar_M = Zj - (Diff / (Tj - TMix))
            else:
                self.ZStar_M = ((DZ / DeltaT) * (TStar_M - Tj)) + Zj

            self.Penerg = (EK + EW) * math.exp(-self.ZStar_M / self.EF_Depth)
            DeltaE = 0

            previous = self.__get_PLayer( self.depths[0] )
            current = self.__get_PLayer( self.depths[1] )
            k = 1
            while k != j:
                DeltaE += (( 0.5 * (current.Density + previous.Density)) - TMix) * (current.Depth - previous.Depth) * (0.5 * (current.Depth + previous.Depth))
                previous = current
                k += 1
                current = self.__get_PLayer( self.depths[k] )

            DeltaE += ((0.5 * (TStar_M + previous.Density)) - TMix) * (self.ZStar_M - previous.Depth) * (0.5 * (self.ZStar_M + previous.Depth))
            if DeltaE > self.Penerg:
                if (((DeltaE - self.Penerg) / self.Penerg) <= 0.001):
                    Finished = 1
            else:
                if (((self.Penerg - DeltaE) / self.Penerg) <= 0.001):
                    Finished = 1
                else:
                    i += 1

        if Finished == 0:
            current = self.__get_PLayer( self.depths[i] )
            Tu = current.Density
            Zu = current.Depth
            next = self.__get_PLayer( self.depths[i+1] )
            To = next.Density
            Zo = next.Depth
            if abs(To-Tu) < 0.000001:
                Finished = 1

        ### Continue from line 511 in original WaterCol.java 
        while Finished == 0:
            TMix = (Tu + To) * 0.5
            ZMix = (Zu + Zo) * 0.5
            current = self.__get_PLayer( self.depths[i] )
            next = self.__get_PLayer( self.depths[i+1] )
            previous = current
            Flob = AFlob + ((0.5 * (ZMix + current.Depth)) * (TMix - current.Density))
            Flun = (next.Density - TMix) * (0.5 * (next.Depth - ZMix))
            j = i + 1

            maxJ = len(self.depths)-1
            while Flun<Flob and j<maxJ:
                previous = next
                j += 1
                next = self.__get_PLayer( self.depths[j] )
                Flun += (next.Density + previous.Density - (2 * TMix)) * (0.5 * (next.Depth - previous.Depth))

            if j >= maxJ and Flun < Flob:
                print "WBPhysics:: Error in Flun<Flob loop. TMix = " + str(TMix)
                maxJ = len(self.depths)
                for j in range(maxJ):
                    _p = self.__get_PLayer( self.depths[j] )
                    print "WBPhysics:: PLayer "+str(j)+", depth = "+str(_p.Depth)+", density = "+str(_p.Density)
                raise Exception("WBPhysics:: Error in Flun<Flob loop")

            m = j
            Tj = next.Density
            TjMin = previous.Density
            Zj = next.Depth
            ZjMin = previous.Depth
            Diff = Flun - Flob
            if i+1 == j: 
                DZ = Zj - ZMix
                DeltaT = Tj - TMix
            else:
                DZ = Zj - ZjMin
                DeltaT = Tj - TjMin

            C = (-2 * Diff * DeltaT ) / DZ
            TStar_M = math.sqrt(C + ((Tj - TMix) * (Tj - TMix))) + TMix
            if DeltaT < 0.000001:
                self.ZStar_M = Zj - (Diff / (Tj - TMix))
            else:
                self.ZStar_M = Zj + ((DZ / DeltaT) * (TStar_M - Tj))
            self.Penerg = (EK+EW) * math.exp(-self.ZStar_M / self.EF_Depth)
            DeltaE = 0

            previous = self.__get_PLayer( self.depths[0] )
            current = self.__get_PLayer( self.depths[1] )
            k = 1
            while k <= i: ##{ /* original: while Old != Current => while k-1 < i) */
                DeltaE += ((0.5 * (current.Density + previous.Density)) - TMix) * (current.Depth - previous.Depth) * (0.5 * (current.Depth + previous.Depth))
                previous = current
                k += 1
                current = self.__get_PLayer( self.depths[k] )

            DeltaE += ((0.5 * (TMix + previous.Density)) - TMix) * (ZMix - previous.Depth) * (0.5 * (ZMix + previous.Depth))
            if k == j:
                DeltaE += ((0.5 * (TMix + TStar_M)) - TMix) * (self.ZStar_M - ZMix) * (0.5 * (self.ZStar_M + ZMix))
            else:
                DeltaE += ((0.5 * (TMix + current.Density)) - TMix) * (current.Depth - ZMix) * (0.5 * (ZMix + current.Depth))

                if k+1 != j:
                    previous = self.__get_PLayer( self.depths[k] )
                    current = self.__get_PLayer( self.depths[k+1] )
                    l = k + 1
                    while l < j:
                        DeltaE += ((0.5*(current.Density + previous.Density)) - TMix) * (current.Depth - previous.Depth) * (0.5 * (current.Depth + previous.Depth))
                        previous = current
                        l += 1
                        current = self.__get_PLayer( self.depths[l] )
                else:
                    previous = self.__get_PLayer( self.depths[k] )
                DeltaE += ((0.5 * (TStar_M + previous.Density)) - TMix) * (self.ZStar_M - previous.Depth) * (0.5 * (self.ZStar_M + previous.Depth));

            if DeltaE > self.Penerg:
                if ((DeltaE - self.Penerg) / self.Penerg) <= 0.001:
                    Finished = 1
                else:
                    To = TMix
                    Zo = ZMix
            else:
                if ((self.Penerg - DeltaE) / self.Penerg) <= 0.001:
                    Finished = 1
                else:
                    Tu = TMix
                    Zu = ZMix
            if abs(Tu - To) < 0.000001:
                Finished=1

        current = self.__get_PLayer( self.depths[0] )
        a = self.C6
        S = current.Salinity
        a3 = a*a*a
        t_a2 = 3*a*a
        b = self.C4+self.C7*S
        b3 = b*b*b
        c = self.C2 + self.C5 * S

        Sigma = TMix
        d = -Sigma + self.C1 + self.C3 * S
        q = 0.5 * ((2 * b3 / (27 * a3)) - (b*c/t_a2) + (d/a))
        p = ((3 * a * c) - (b*b)) / (t_a2 * 3.0)
        r = -math.sqrt(abs(p))
        PHI = math.acos(q/(r*r*r))
        y3 = 2 * r * math.cos((self.Pi / 3.0) + (PHI / 3.0))
        T = y3 - (b / (3.0 * a))
        self.MixTemp = T

        Sigma = TStar_M
        d = -Sigma + self.C1 + self.C3 * S
        q = 0.5 * ((2 * b3 / (27.0 * a3)) - (b*c/t_a2)+(d/a))
        p = ((3 * a * c) - (b*b)) /(t_a2*3)
        r = -math.sqrt(abs(p))
        PHI = math.acos(q/(r*r*r))
        y3 = 2*r*math.cos((self.Pi/3) + (PHI/3))
        T = y3 - (b / (3.0 * a))
        TempStar_M = T

        k = 0
        while k!=j:
            current.Density = TMix
            current.Temperature = self.MixTemp
            k += 1
            current = self.__get_PLayer( self.depths[k] )

        S = current.Salinity
        Sigma = current.Density
        previous = self.__get_PLayer( self.depths[k-1] )
        CPLayer = current
        b = self.C4 + self.C7 * S
        b3 = b*b*b
        c = self.C2 + self.C5 * S
        d = -Sigma + self.C1 + self.C3 * S
        q = 0.5*((2.0*b3/(27.0*a3))-(b*c/t_a2)+(d/a))
        p = ((3.0*a*c)-(b*b))/(t_a2*3.0)
        r = -math.sqrt(abs(p))
        PHI = math.acos(q/(r*r*r))
        y3 = 2.0*r*math.cos((self.Pi/3.0)+(PHI/3.0))
        T = y3 - (b/(3.0*a))
        current.Temperature = T
        DZZMN = self.ZStar_M - previous.Depth
        DZNZM = current.Depth - self.ZStar_M

        if DZZMN >= 0.0002 or DZNZM >= 0.0002:
            if DZNZM < 0.0002:
                if DZZMN < 0.0004:
                    current = PLayer( CPLayer.Depth - 0.0001 )
                    self.__insert_PLayer( current, k )
                    k += 1
                else:
                    current = PLayer( CPLayer.Depth - 0.0002 )
                    self.__insert_PLayer( current, k )
                    k += 1
                current.Density = TMix
                current.Temperature = self.MixTemp
                current.TempAdj = CPLayer.TempAdj
                current.Salinity = CPLayer.Salinity
                current.Full_Irrad = CPLayer.Full_Irrad
                current.Vis_Irrad = CPLayer.Vis_Irrad
            elif DZZMN < 0.0002:
                if DZNZM < 0.0004:
                    current = PLayer( previous.Depth + 0.0001 )
                    self.__insert_PLayer( current, k )
                    k += 1
                else:
                    current = PLayer( previous.Depth + 0.0002 )
                    self.__insert_PLayer( current, k )
                    k += 1
                current.Density = TStar_M
                current.Temperature = TempStar_M
                current.TempAdj = previous.TempAdj
                current.Salinity = previous.Salinity
                current.Full_Irrad = previous.Full_Irrad
                current.Vis_Irrad = previous.Vis_Irrad
            else:
                current = PLayer( self.ZStar_M - 0.0001 )
                self.__insert_PLayer( current, k )
                k += 1
                current.Density = TMix
                current.Temperature = self.MixTemp
                current.TempAdj = (CPLayer.TempAdj+previous.TempAdj)/2.0
                current.Salinity = ((CPLayer.Salinity - previous.Salinity) * (current.Depth - previous.Depth) / (CPLayer.Depth - previous.Depth)) + previous.Salinity
                current.Full_Irrad = (CPLayer.Full_Irrad + previous.Full_Irrad) /2.0
                current.Vis_Irrad = (CPLayer.Vis_Irrad + previous.Vis_Irrad) /2.0
                previous = current

                current = PLayer(self.ZStar_M + 0.0001)
                self.__insert_PLayer( current, k )
                current.Density = TStar_M
                
                current.Temperature = TempStar_M
                current.TempAdj = previous.TempAdj
                current.Salinity = previous.Salinity
                current.Full_Irrad = previous.Full_Irrad
                current.Vis_Irrad = previous.Vis_Irrad

        for k in range(m+1,len(self.depths)):
            current = self.__get_PLayer( self.depths[k] )
            S = current.Salinity
            Sigma = current.Density
            b = self.C4 + self.C7 * S
            b3 = b*b*b
            c = self.C2 + self.C5 * S
            d = -Sigma + self.C1 + self.C3 * S
            q = 0.5 * ((2 * b3 / (27 * a3)) - (b*c/t_a2) + (d/a))
            p = ((3 * a * c) - (b * b)) / (t_a2 * 3)
            r = -math.sqrt (abs(p))
            PHI = math.acos (q/(r*r*r))
            y3 = 2*r*math.cos((self.Pi/3.0) + (PHI / 3.0))
            T = y3 - (b/(3*a))
            current.Temperature = T

    def Order(self):
        i = j= 1
        ref_d = self.ref_depths[0]
        current = self.__get_PLayer( self.depths[0] )

        while i < len(self.ref_depths):
            ref_d = self.ref_depths[i]
            current = self.__get_PLayer( self.depths[j] )
            if round(ref_d, self.dtol) != round(current.Depth, self.dtol):
                next = self.__get_PLayer( self.depths[j+1] )
                if current.Density == next.Density: #// || (current.Density != ((PLayer) P_LayerList.get(j-1)).Density))
                    del self.layers[ round(self.depths[j], self.dtol) ]
                    self.depths = np.delete(self.depths, j)                    
                    j -= 1
                i -= 1
            else:                
                pass
            i += 1
            j += 1

