input: clean
#	cp ../../meshes/lerm_nat* .
	cp ../../meshes/lerm_1m* .
	cp ../../wb-physics/WBPhysics.py .
	cp ../../wb-physics/lerm_physics_init.bin .
	cp ../../wb-physics/lerm_bcds.bin .
	cp ../../wb-physics/LERM_InitialConditions.csv .

clean:
	rm -rf *.halo *.vtu *.pvtu *.log* *.err* *.stat *.detectors *.detectors.dat
	rm -f *.msh *.halo *.nc *.geo *.pyc
	rm -rf vew_toymodel_*
	rm -rf functional_group functional_group.so setup_ext.py __init__.py build
